package csc251.util;


import groovy.json.internal.ArrayUtils;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;

public class CustomArrayList<E extends Object> {

    private static final Object[] EMPTY_ELEMENTDATA = {};

    private Object[] container; // non-private to simplify nested class access

    private static final int DEFAULT_CAPACITY = 10;

    private int size;

    private int growCount;

    public CustomArrayList() {
        size = 0;
        this.container = EMPTY_ELEMENTDATA;
    }

    public CustomArrayList(int initialCapacity) {
        size = 0;
        if (initialCapacity > 0) {
            this.container = new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            this.container = EMPTY_ELEMENTDATA;
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
    }

    // Size Control

    public int growth() {
        return growCount;
    }

    private void ensureCapacity(int minCapacity) {
        if (minCapacity - container.length > 0)
            grow(minCapacity);
    }
    
    private void grow(int minCapacity) {

        growCount++;
        int oldCapacity = container.length;

        int newCapacity = oldCapacity + (oldCapacity / 2);

        if (newCapacity < DEFAULT_CAPACITY)
            newCapacity = DEFAULT_CAPACITY;

        if (newCapacity < minCapacity)
            grow(newCapacity);

        // minCapacity is usually close to size, so this is a win:
        container = Arrays.copyOf(container, newCapacity);
    }

    public void clear() {
        container = EMPTY_ELEMENTDATA;
        size = 0;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void add(E element) {
        ensureCapacity(size + 1);  // Increments growthCount!!
        container[size++] = element;
    }

    public void add(int index, E element) {
        ensureCapacity(size + 1);  // Increments growthCount!!
        container[size++] = element;
    }

    // TODO - Make Fast
    public void add(E... elements) {
        for (E element : elements) {
            add(element);
        }
    }

    public void addAll(int index, Collection<? extends E> items) {
        ensureCapacity(size + items.size());
        for (E item : items) {
            container[index++] = item;
            size++;
        }
    }

    public void addAll(Collection<? extends E> items) {
        for (E item : items) {
            add(item);
        }
    }

    public boolean contains(E value) {
        for (Object element : container)
            if (element == value || value != null && value.equals(element))
                return true;
        return false;
    }

    public boolean contains(Collection<? extends E> items) {
        boolean hasAll = false;
        for (E item : items) {
            hasAll = contains(item);
        }
        return hasAll;
    }

    public int indexOf(E target) {
        for (int i = 0; i < size; i++) {
            if (container[i] == target)
                return i;
        }
        return -1;
    }

    public int lastIndexOf(E target) {
        int last = -1;
        for (int i = 0; i < size; i++) {
            if (container[i] == target)
                last = i;
        }
        return last;
    }

    public Object get(int index) {
        if (index >= size || index < 0)
            throw new IndexOutOfBoundsException();
        return container[index];
    }

    public void remove(E target) {
        int index = indexOf(target);
        if (index != -1) {
            container[index] = null;
            if (index == size - 1) {
                System.arraycopy(container, 0, container, 0, --size - 1);
            } else {
                System.arraycopy(container, index + 1, container, index, --size - index - 1);
            }
        }
    }

    public void remove(int index) {
        if (index < size) {
            container[index] = null;
            if (index == size - 1) {
                System.arraycopy(container, 0, container, 0, --size - 1);
            } else {
                System.arraycopy(container, index + 1, container, index, --size - index - 1);
            }
        }
    }

    public Object[] toArray() {
        return Arrays.copyOf(container, size);
    }
}
