package csc251.util;

import csc251.DateRange;

import java.time.LocalDate;
import java.util.Collection;

public class DateRangeUtil {

    private DateRangeUtil() {}


    public static boolean hasOverlap(DateRange needle, Collection<DateRange> haystack) {
        for (DateRange range : haystack) {
            if (hasOverlap(needle , range))
                return true;
        }
        return false;
    }

    public static boolean hasOverlap(DateRange first, DateRange second) {
        boolean hasOverlap = false;

            // If start of first AFTER end of second OR end of first BEFORE start of second
        if ( first.getStart().isAfter(second.getEnd()) || first.getEnd().isBefore(second.getStart()) ) {
            hasOverlap = false;

            // If start of first BEFORE end of second OR end of first AFTER start of second
        } else if (first.getStart().isBefore(second.getEnd()) || first.getEnd().isAfter(second.getStart())) {
            hasOverlap = true;

            // If same range
        } else if (first.getStart().isEqual(second.getStart()) || first.getEnd().isEqual(second.getEnd())) {
            hasOverlap = true;
        }

        return hasOverlap ;
    }

    public static boolean contains(DateRange range, LocalDate... dates) {
        for (LocalDate date : dates) {
            if (contains(range , date))
                return true;
        }
        return false;
    }

    public static boolean contains(DateRange range, LocalDate date) {
        if (range.getStart().isBefore(date) && range.getEnd().isAfter(date)) {
            return true;
        } else if (range.getStart().isEqual(date) || range.getEnd().isEqual(date)) {
            return true;
        }
        return false;
    }





}
