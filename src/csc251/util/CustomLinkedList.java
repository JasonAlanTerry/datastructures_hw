package csc251.util;

import java.util.Collection;

public class CustomLinkedList<E> {

    private static final class Entry {
        private Entry next;
        private Entry prev;
        private Object data;

        Entry() {
            next = null;
            prev = null;
            data = null;
        }

        Entry(Object obj){
            next = null;
            prev = null;
            data = obj;
        }

    }

    private Entry head;
    private Entry tail;
    private int size;

    public CustomLinkedList() {
        head = new Entry();
        tail = head;
        size = 0;
    }

    public int size() { return size; }

    public boolean isEmpty() {
        return size == 0;
    }

    // TODO -- Verify correctness
    public void add(E element) {
        Entry e = new Entry(element);
        if(isEmpty()) {

            head = e;
            e.next = null;

            tail = e;
            e.prev = null;

        } else {

            last().next = e;
            e.prev = last();
            tail = e;
        }

        size++;
    }

    public void set(int index, E element) {
        int split = 0;
        Entry itr = head;
        if(index < size() && index > -1) {
            while (split != index && itr != null) {
                itr = itr.next;
                split++;
            }

            // set bounds
            size++;
            Entry e = new Entry(element);
            Entry prev = itr.prev;
            Entry next = itr;
            // add to head
            if (prev == null) {
                head = e;
                e.prev = prev;
                e.next = next;
                e.next.prev = e;
            }
            // add to tail
            else if (next.next == null) {
                tail = next;
                e.prev = prev;
                e.next = next;
                e.prev.next = e;

            }
            // add to split
            else {
                e.prev = prev;
                e.next = next;
                e.next.prev = e;
                e.prev.next = e;
            }
        }
    }

    // TODO -- ????
    public void addAll(int index, Collection<? extends E> items) {
        if(index < size() && index > -1) {
            for (E item : items) {
                set(index, item);
                index++;
            }
        }
    }

    public void addAll(Collection<? extends E> items) {
        for (E item : items) {
            add(item);
        }
    }

    public String print() {
        String str = "";
        Entry itr = head;
        while (itr != null) {
            str += itr.data + " ";
            itr = itr.next;
        }
        return str;
    }

    private Entry first() {
        return head;
    }

    private Entry last() {
        return tail;
    }

    public void clear() {
        size = 0;

        Entry itr = head;
        while (itr != null) {
            itr.data = null;
            itr = itr.next;
        }

        head.next = null;
        head.prev = null;
        tail.next = null;
        tail.prev = null;
    }

    // How could this ever not work with primitives vs Objects?
    public boolean contains(Object target) {
        Entry itr = head;
        while (itr != null) {
            if (itr.data == target) {
                return true;
            }
            itr = itr.next;
        }
        return false;
    }

    public boolean containsAll(Collection<? extends E> items) {
        boolean hasAll = false;
        for (E item : items) {
            hasAll = contains(item);
        }
        return hasAll;
    }

    // Best method to use??
    public E get(int index) {
        Entry itr = head;
        for (int i = 1; i <= index; i++ ) {
            itr = itr.next;
        }
        return (E) itr.data;
    }

    // TODO -- Verify correctness
    public void remove(Object item) {
        Entry itr = head;
        Entry target = new Entry();
        // go git it
        while (itr != null) {
            itr = itr.next;
            if (itr.data.equals(item)) {
                target = itr;
                itr = null;
            }
        }

        target.data = null;

        if (target == first()) {
            head = target.next;
            target.next.prev = null;
        }

        if (target == last()) {
            tail = target.prev;
            tail.next = null;
        }

        target.next.prev = target.prev;
        target.prev.next = target.next;
        size--;

    }

    public int indexOf(Object item) {
        int index = 0;
        Entry itr = head;
        while (itr != null) {
            if (itr.data.equals(item))
                return index;
            itr = itr.next;
            index++;
        }
        return -1;
    }

    public int lastIndexOf(Object item) {
        int index = 0;
        int last = -1;
        Entry itr = head;
        while (itr != null) {
            if (itr.data.equals(item))
                last = index;
            itr = itr.next;
            index++;
        }
        return last;
    }
}
