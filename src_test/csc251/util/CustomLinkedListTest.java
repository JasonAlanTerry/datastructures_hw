package csc251.util;

import org.hamcrest.core.Is;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CustomLinkedListTest {

    @Test
    public void expect_empty_LinkedList_when_new_LinkedList_created() {
        CustomLinkedList linkedListTest = new CustomLinkedList();
        assertTrue(linkedListTest.isEmpty());
    }

    @Test
    public void given_new_elements_should_add_in_sequence_to_end_of_list() {
        CustomLinkedList linkedListTest = new CustomLinkedList();
        linkedListTest.add(12);
        linkedListTest.add(24);
        linkedListTest.add(44);
        linkedListTest.add(222);
        System.out.println(linkedListTest.print());
        assertTrue(linkedListTest.size() == 4);
    }

    @Test
    public void given_a_target_return_true_if_in_list_false_if_not() {
        CustomLinkedList<Integer> linkedListInt = new CustomLinkedList<>();
        CustomLinkedList<Object> linkedListObj = new CustomLinkedList<>();

        linkedListInt.add(12);
        linkedListInt.add(24);
        linkedListInt.add(44);

        Object target = new Object();

        linkedListObj.add(new Object());
        linkedListObj.add(target);
        linkedListObj.add(new Object());

        assertTrue(linkedListInt.contains(44));
        assertFalse(linkedListInt.contains(90192));
        assertTrue(linkedListObj.contains(target));
        assertFalse(linkedListObj.contains("Hello"));

    }

    @Test
    public void given_a_collection_add_collection_to_end_of_linked_list() {
        CustomLinkedList<Integer> linkedListInt = new CustomLinkedList<>();
        ArrayList<Integer> arrayList = new ArrayList<>();
        List<Integer> expected = new ArrayList<>(Arrays.asList(12,24,44,1,324,65));
        arrayList.add(1);
        arrayList.add(324);
        arrayList.add(65);
        linkedListInt.add(12);
        linkedListInt.add(24);
        linkedListInt.add(44);

        linkedListInt.addAll(arrayList);
        assertTrue(linkedListInt.containsAll(expected));
        assertTrue(linkedListInt.containsAll(arrayList));
    }

    // TODO -- Ask Chuck --
    @Test
    public void given_index_and_collection_insert_collection_into_linked_list() {
        CustomLinkedList<Integer> linkedListInt = new CustomLinkedList<>();
        ArrayList<Integer> arrayList = new ArrayList<>();
        List<Integer> expected = new ArrayList<>(Arrays.asList(12, 24, 1, 324, 65, 44));
        arrayList.add(1);
        arrayList.add(324);
        arrayList.add(65);
        linkedListInt.add(12);
        linkedListInt.add(24);
        linkedListInt.add(44);

        linkedListInt.addAll(2, arrayList);
        assertTrue(linkedListInt.containsAll(expected));
        assertTrue(linkedListInt.containsAll(arrayList));
    }

    @Test
    public void given_index_and_item_insert_item_into_linked_list_at_correct_position() {
        CustomLinkedList<Integer> linkedListInt = new CustomLinkedList<>();
        List<Integer> expected = new ArrayList<>(Arrays.asList(13 ,12, 24, 75, 44));
        linkedListInt.add(12);
        linkedListInt.add(24);
        linkedListInt.add(44);

        linkedListInt.set(2, 75); // add to tail
        linkedListInt.set(0, 13); // add to head wrks
        linkedListInt.set(5, 77); // add split

        assertTrue(linkedListInt.containsAll(expected));
        assertTrue(linkedListInt.indexOf(13) == 0);
        assertTrue(linkedListInt.indexOf(75) == 3);
        assertFalse(linkedListInt.contains(77));
    }

    @Test
    public void given_index_in_range_of_list_return_node_data_as_desired_type() {
        CustomLinkedList<Integer> linkedListInt = new CustomLinkedList<>();
        CustomLinkedList<String> linkedListStr = new CustomLinkedList<>();

        linkedListInt.add(12);
        linkedListInt.add(24);
        linkedListInt.add(44);

        linkedListStr.add("HELLO");
        linkedListStr.add("HANK HILL");
        linkedListStr.add("DAMN'IT BOBBY");

        assertTrue(linkedListInt.contains(24));
        int returnedInt = linkedListInt.get(1);
        assertTrue(returnedInt == 24);

        assertTrue(linkedListStr.contains("DAMN'IT BOBBY"));
        String returnedStr = linkedListStr.get(2);
        assertTrue(returnedStr.equals("DAMN'IT BOBBY"));
    }

    @Test
    public void given_target_find_and_remove_first_instance_of_target_from_list() {
        CustomLinkedList<Integer> linkedListInt = new CustomLinkedList<>();

        linkedListInt.add(12);
        linkedListInt.add(24);
        linkedListInt.add(44);

        System.out.println(linkedListInt.print());
        assertTrue(linkedListInt.contains(24));
        linkedListInt.remove(24);
        assertFalse(linkedListInt.contains(24));
        System.out.println(linkedListInt.print());
    }

    @Test
    public void given_list_when_cleared_expect_empty_list() {
        CustomLinkedList<Integer> linkedListInt = new CustomLinkedList<>();

        linkedListInt.add(12);
        linkedListInt.add(24);
        linkedListInt.add(44);

        assertTrue(linkedListInt.contains(12));
        assertTrue(linkedListInt.contains(44));

        System.out.println(linkedListInt.print());
        linkedListInt.clear();
        System.out.println(linkedListInt.print());

        assertFalse(linkedListInt.contains(12));
        assertFalse(linkedListInt.contains(44));
    }

    @Test
    public void given_object_return_indexOf_object_within_the_linked_list() {
        CustomLinkedList<Integer> linkedListInt = new CustomLinkedList<>();

        linkedListInt.add(12);
        linkedListInt.add(24);
        linkedListInt.add(44);

        assertTrue(linkedListInt.indexOf(12) == 0);
        assertTrue(linkedListInt.indexOf(24) == 1);
        assertTrue(linkedListInt.indexOf(44) == 2);
    }

    @Test
    public void given_object_return_lastIndexOf_object_in_linked_list() {
        CustomLinkedList<Integer> linkedListInt = new CustomLinkedList<>();

        linkedListInt.add(12);
        linkedListInt.add(24);
        linkedListInt.add(44);
        linkedListInt.add(12);

        assertTrue(linkedListInt.lastIndexOf(12) == 3);
    }
}
