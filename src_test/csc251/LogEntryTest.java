package csc251;

import org.junit.Test;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.*;


public class LogEntryTest {

    @Test
    public void should_return_string_when_getName_called() throws Exception {
        LogEntry logEntryFirst = new LogEntry();
        LogEntry logEntrySecond = new LogEntry("HANK");
        assertThat(logEntryFirst.getName(), instanceOf(String.class));
        assertThat(logEntrySecond.getName(), instanceOf(String.class));
    }

    @Test
    public void should_return_type_LocalDate_when_getStart_called() throws Exception {
        LogEntry logEntryFirst = new LogEntry();
        LogEntry logEntrySecond = new LogEntry("HANK");
        assertThat(logEntryFirst.getStart(), instanceOf(LocalDate.class));
        assertThat(logEntrySecond.getStart(), instanceOf(LocalDate.class));
    }

    @Test
    public void should_return_type_LocalDate_when_getEnd_called() throws Exception {
        LogEntry logEntryFirst = new LogEntry();
        LogEntry logEntrySecond = new LogEntry("HANK");
        assertThat(logEntryFirst.getStart(), instanceOf(LocalDate.class));
        assertThat(logEntrySecond.getStart(), instanceOf(LocalDate.class));
    }

}