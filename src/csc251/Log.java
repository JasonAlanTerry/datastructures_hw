package csc251;

import java.util.ArrayList;
import java.util.List;

public class Log {

    private List<LogEntry> logEntries;

    public Log() {
        logEntries = new ArrayList<>(); // WHY?!?!
    }

    public void addLogEntry(LogEntry entry) {
        logEntries.add(entry);
    }

    public List<LogEntry>  getLogEntries() {
        return new ArrayList(logEntries);
    }
}
