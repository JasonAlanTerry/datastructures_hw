package csc251;

import java.time.LocalDate;

public interface DateRange {

    LocalDate getStart();

    LocalDate getEnd();

}
