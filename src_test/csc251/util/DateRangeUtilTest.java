package csc251.util;

import csc251.DateRange;
import csc251.Log;
import csc251.LogEntry;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Collection;

import static org.junit.Assert.*;

// Purpose
// Test our utility class on our LogEntry class
// Test LogEntry's stored in Log's against other Logs
public class DateRangeUtilTest {


    private static DateRangeUtil dateRangeUtil;
    private static Log testLog;
    private static LogEntry rangeOne;
    private static LogEntry rangeTwo;
    private static LogEntry rangeThree;

    @BeforeClass
    public static void setUp() throws Exception {
        testLog = new Log();
    }

    @Test
    public void should_return_true_when_two_DateRanges_contain_any_of_same_Days() throws Exception {

        LocalDate today = LocalDate.now();
        LocalDate tomorrow = today.plusDays(1);
        LocalDate week_later = LocalDate.now().plusDays(7);
        LocalDate week_after_tomorrow = tomorrow.plusDays(7);

        rangeOne = new LogEntry("ONE",today, week_later);
        rangeTwo = new LogEntry("TWO", tomorrow, week_after_tomorrow);

        // Given two date ranges, assert that overlap
        assertTrue(DateRangeUtil.hasOverlap(rangeOne, rangeTwo ));

    }

    @Test
    public void should_return_false_when_two_DateRanges_do_not_contain_any_of_same_Days() throws Exception {

        LocalDate today = LocalDate.now();
        LocalDate tomorrow = today.plusDays(1);
        LocalDate week_later = LocalDate.now().plusDays(7);
        LocalDate week_after_tomorrow = tomorrow.plusDays(7);

        rangeOne = new LogEntry("ONE",today, tomorrow);
        rangeTwo = new LogEntry("TWO", week_later, week_after_tomorrow);

        // Given two date ranges, assert that overlap
        assertFalse(DateRangeUtil.hasOverlap(rangeOne, rangeTwo ));

    }

    @Test
    public void should_return_true_when_DateRange_Collection_contains_any_common_days_given_DateRange() throws Exception {

        LocalDate today = LocalDate.now();
        LocalDate tomorrow = today.plusDays(1);
        LocalDate week_later = LocalDate.now().plusDays(7);
        LocalDate week_after_tomorrow = tomorrow.plusDays(7);

        rangeOne = new LogEntry("ONE",today, tomorrow);
        rangeTwo = new LogEntry("TWO", tomorrow, week_after_tomorrow);
        rangeThree = new LogEntry("THREE", today, week_later);

        testLog.addLogEntry(rangeOne);
        testLog.addLogEntry(rangeTwo);

        Collection<DateRange> collection = (Collection) testLog.getLogEntries();

        // Given two date ranges, assert that overlap
        assertTrue(DateRangeUtil.hasOverlap(rangeThree, collection ));

    }

    @Test
    public void should_return_false_when_DateRange_Collection_contains_no_common_days_as_given_DateRange() {
        LocalDate today = LocalDate.now();
        LocalDate tomorrow = today.plusDays(1);
        LocalDate week_later = LocalDate.now().plusDays(7);
        LocalDate week_after_tomorrow = tomorrow.plusDays(7);

        rangeOne = new LogEntry("ONE",today, tomorrow);
        rangeTwo = new LogEntry("TWO", tomorrow, week_later);
        rangeThree = new LogEntry("THREE", week_after_tomorrow, week_after_tomorrow);

        testLog.addLogEntry(rangeOne);
        testLog.addLogEntry(rangeTwo);

        Collection<DateRange> collection = (Collection) testLog.getLogEntries();

        // Given two date ranges, assert that overlap
        assertFalse(DateRangeUtil.hasOverlap(rangeThree, collection ));
    }

    @Test
    public void should_return_true_when_DateRange_contains_any_of_given_Dates() {
        LocalDate today = LocalDate.now();
        LocalDate tomorrow = today.plusDays(1);
        LocalDate week_later = LocalDate.now().plusDays(7);
        LocalDate week_after_tomorrow = tomorrow.plusDays(7);

        rangeOne = new LogEntry("ONE",today, week_after_tomorrow);

        assertTrue(DateRangeUtil.contains(rangeOne, week_later));
        assertTrue(DateRangeUtil.contains(rangeOne, week_later, tomorrow));
    }

    @Test
    public void should_return_false_when_DateRange_contains_none_of_given_Dates() {
        LocalDate today = LocalDate.now();
        LocalDate tomorrow = today.plusDays(1);
        LocalDate week_later = LocalDate.now().plusDays(7);
        LocalDate week_after_tomorrow = tomorrow.plusDays(7);

        rangeOne = new LogEntry("ONE",today, tomorrow);

        assertFalse(DateRangeUtil.contains(rangeOne, week_later));
        assertFalse(DateRangeUtil.contains(rangeOne, week_later, week_after_tomorrow));
    }

}