package csc251.util;

import org.hamcrest.core.Is;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class CustomArrayListTest {

    @Test
    public void expect_new_CustomList_to_be_of_size_zero() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        assertTrue(testListObject.isEmpty());
    }

    @Test
    public void expect_CustomList_to_be_size_zero_when_Cleared() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object element = new Object();

        testListObject.add(element);
        testListObject.add(element);

        assertTrue(testListObject.size() > 0);

        testListObject.clear();

        assertTrue(testListObject.isEmpty());
    }

    @Test
    public void given_new_Element_assert_it_has_been_added_to_CustomList() {
        CustomArrayList<String> testListObject = new CustomArrayList<>();
        String element = "Test";
        assertTrue(testListObject.size() < 1);
        testListObject.add(element);
        assertTrue(testListObject.size() == 1);
    }

    @Test
    public void given_40_new_Elements_assure_CustomList_grows() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object element = new Object();
        for (int i = 0; i < 40; i++)
            testListObject.add(element);

        assertTrue(testListObject.size() == 40);
        assertTrue(testListObject.growth() > 0);
    }

    @Test
    public void given_multiple_Elements_assert_they_have_be_added_to_CustomList() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();
        testListObject.add(a, b);
        assertTrue(testListObject.contains(a));
        assertTrue(testListObject.contains(b));
    }

    @Test
    public void given_Object_check_if_CustomList_contains_Object() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();
        testListObject.add(a, b);
        assertTrue(testListObject.contains(a));
    }

    @Test
    public void given_group_of_Objects_check_if_CustomList_contains_all_Objects() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();
        testListObject.add(a, b);

        ArrayList<Object> expected = new ArrayList<>();
        expected.add(a);
        expected.add(b);

        ArrayList<String> notExpected = new ArrayList<>();
        notExpected.add("Nope");

        assertTrue(testListObject.contains(expected));
        assertFalse(testListObject.contains(notExpected));
    }

    @Test
    public void given_Object_find_and_remove_Object_in_CustomList() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();
        testListObject.add(a, b);
        assertTrue(testListObject.contains(b));
        assertTrue(testListObject.size() == 2);
        testListObject.remove(b);
        assertFalse(testListObject.contains(b));
        assertTrue(testListObject.size() == 1);
    }

    @Test
    public void expect_Get_to_return_desired_object_at_index() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();
        testListObject.add(a, b);
        Object c = testListObject.get(1);
        assertThat(c, Is.is(b));
    }

    @Test
    public void given_Object_return_index_of_Object() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();
        testListObject.add(a, b, a);
        int indexOfB = 1;
        assertTrue(indexOfB == testListObject.indexOf(b));
    }

    @Test
    public void given_Object_return_last_index_of_Object() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();
        testListObject.add(a, b, a);
        int lastIndexOfA = 2;
        assertTrue(lastIndexOfA == testListObject.lastIndexOf(a));
    }

    @Test
    public void expect_newArray_to_return_exact_copy_of_CustomList_as_primitive_array() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();
        Object[] expectedObj = {a, b};
        testListObject.add(a, b);
        Object[] asArrayObj = testListObject.toArray();
        assertArrayEquals(asArrayObj, expectedObj);
    }

    @Test
    public void expect_addAll_to_add_Collection_to_CustomList() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();

        testListObject.add(a, b); // {a, b}

        ArrayList<Object> toAdd = new ArrayList<>(); // {a, b}
        toAdd.add(a);
        toAdd.add(b);

        testListObject.addAll(toAdd);

        ArrayList<Object> expected = new ArrayList<>(); // {a, b, a, b}
        expected.add(a);
        expected.add(b);
        expected.add(a);
        expected.add(b);

        assertTrue(testListObject.contains(expected));
    }

    @Test
    public void expect_addAll_to_add_Collection_to_CustomList_at_index_given() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();

        testListObject.add(a, b); // {a, b}

        ArrayList<Object> toAdd = new ArrayList<>(); // {a, b}
        toAdd.add(a);
        toAdd.add(b);

        testListObject.addAll(1 ,toAdd);

        ArrayList<Object> expected = new ArrayList<>(); // {a, a, b, b}
        expected.add(a);
        expected.add(a);
        expected.add(b);
        expected.add(b);

        assertTrue(testListObject.contains(expected));
    }

    @Test
    public void expect_Object_at_index_to_be_removed_and_size_adjusted() {
        CustomArrayList<Object> testListObject = new CustomArrayList<>();
        Object a = new Object();
        Object b = new Object();
        testListObject.add(a, b);
        assertTrue(testListObject.contains(b));
        assertTrue(testListObject.size() == 2);
        testListObject.remove(1);
        assertFalse(testListObject.contains(b));
        assertTrue(testListObject.size() == 1);
    }
}