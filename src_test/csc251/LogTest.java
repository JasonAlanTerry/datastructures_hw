package csc251;

import org.hamcrest.core.Is;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.junit.Assert.*;



public class LogTest {

    // contain list of LogEntries
    // add new LogEntries to list
    // get the list of LogEntries

    // Is it wrong to make these static??
    private static Log logTest;

    @BeforeClass
    public static void setUp() {
        logTest = new Log();
        logTest.addLogEntry(new LogEntry("HANK"));
        logTest.addLogEntry(new LogEntry("WILL"));
        logTest.addLogEntry(new LogEntry("DEBBIE"));
        logTest.addLogEntry(new LogEntry("HOLLY"));
    }

    @Test
    public void assert_getLogEntries_returns_expected_list_type_ArrayList() {
        ArrayList<LogEntry> testList = (ArrayList) logTest.getLogEntries();
        assertSame(logTest.getLogEntries().getClass(), testList.getClass());
    }

}