package csc251;

import java.time.LocalDate;

public class LogEntry implements DateRange {

    public String name;
    public LocalDate checkin;
    public LocalDate checkout;

    public LogEntry() {
        name = "NO_NAME";
        checkin = LocalDate.now();
        checkout = checkin.plusDays(1);
    }

    public LogEntry(String subject) {
        name = subject;
        checkin = LocalDate.now();
        checkout = checkin.plusDays(1);
    }

    public LogEntry(String subject, LocalDate in, LocalDate out) {
        name = subject;
        checkin = in;
        checkout = out;
    }

    public String getName() {
        return name;
    }

    public LocalDate getStart() {
        return checkin;
    }

    public LocalDate getEnd() {
        return checkout;
    }

}
