package csc251.util;

public class ArrayUtil {

    private ArrayUtil() {

    }

    public static void fill(int[] toFill, int val) {

        for (int i = 0; i < toFill.length; i++) {
            toFill[i] = val;
        }
    }

    public static int[] copyOfRange(int[] original, int first, int last) {
        int[] copy = new int[last - first + 1];
        // starting with the FIRST requested index.
        // iterate through to the LAST requested index
        // copy values to new array.
        // o(n) ?

        for (int i = 0; i < copy.length; i++) {
            copy[i] = original[i + first];
        }

        return copy;
    }

    public static int sum(int[] addends) {
        int sum = 0;

        for (int addend : addends) {
            sum += addend;
        }

        return sum;
    }

    public static int sum(int[][] addends) {
        int sum = 0;

        for (int[] addend : addends) {
            for (int anAddend : addend) {
                sum += anAddend;
            }
        }

        return sum;
    }

    public static void print(int[] array) {
        System.out.print("|");
        for(int i : array) {
            System.out.print(i + " ");
        }
        System.out.print("|");
        System.out.println();
    }

    public static void print(int[][] array2d) {
        System.out.print("|");
        for(int[] anArray : array2d) {
            for ( int i : anArray ) {
                System.out.print(i + " ");
            }
            System.out.print("|");
        }
        System.out.println();
    }

}
