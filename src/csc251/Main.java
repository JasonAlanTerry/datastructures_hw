package csc251;

import csc251.util.ArrayUtil;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here


        int[] array = {6, 5, 4, 3, 2, 1};
        int[][] array2d = {{1, 2}, {3, 4}, {5, 6}};
        int[] arrayCopy = ArrayUtil.copyOfRange(array, 2, array.length - 1);

        System.out.println("Contents of Array");
        ArrayUtil.print(array);
        System.out.println("Contents of ArrayCopy");
        ArrayUtil.print(arrayCopy);
        System.out.println("Contents of Array after fill");
        ArrayUtil.fill(array, 12);
        ArrayUtil.print(array);



        System.out.println("\n -- Sums --");

        ArrayUtil.print(array);
        System.out.println(ArrayUtil.sum(array));

        ArrayUtil.print(array2d);
        System.out.println(ArrayUtil.sum(array2d));

        // ---

        Log log = new Log();

        log.addLogEntry(new LogEntry());
        log.addLogEntry(new LogEntry("SomeDude"));

        ArrayList<LogEntry> logs = (ArrayList<LogEntry>) log.getLogEntries();

    }
}
